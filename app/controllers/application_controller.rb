class ApplicationController < ActionController::Base
  protect_from_forgery

  def is_allow? options = {}, user_id
    (return true if current_user.admin) if :for_admin
    (return true if current_user.id == user_id || current_user.admin) if :for_user
  end

end
